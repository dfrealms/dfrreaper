package me.TEKAA92.DFRReaper;

import java.io.Serializable;

public class DFRPlayer implements Serializable {
private static final long serialVersionUID = 1L;
	
	private String name;
	private String race;
	private double hardinessE;
	private double hardinessP;
	private double lvlPlayer;
	private int knockoutCounter;
	
	DFRPlayer(String name, double hardinessE, double hardinessP, int lvlPlayer, String race, int knockoutCounter) {
		
		this.name = name;
		this.hardinessE = hardinessE;
		this.hardinessP = hardinessP;
		this.lvlPlayer = lvlPlayer;
		this.race = race;
		this.knockoutCounter = knockoutCounter;
	}
	
	public String getDFRPlayerName() {
		return name;
	}
	public String getDFRRace() {
		return race;
	}
	
	public double getHardinessE() {
		return hardinessE;
	}
	
	public double getHardinessP() {
		return hardinessP;
	}
	public int getlvlPlayer() {
		return ((int) Math.ceil(lvlPlayer / 100.0));
	}
	
	public void setHardinessE(double death) {		
		hardinessE = hardinessE - death; 	
	}
	public void setHardinessP(double death) {		
		hardinessP = hardinessP - death; 	
	}
	public void hardinessElvl(double lvlE) {
		hardinessE = hardinessE + lvlE;
	}
	public void hardinessPlvl(double lvlP) {
		hardinessP = hardinessP + lvlP;
	}
	public void lvlPlayerUp(int lvlUp) {
		lvlPlayer = lvlUp;
	}
	public int lvlPlayerDeath(double lvlDeath) {
		lvlPlayer =((int) Math.ceil(lvlPlayer/lvlDeath) / 100.0);
		return ((int) Math.ceil(lvlPlayer / 100.0));
	}
	public void setRace(String newRace){
		race = newRace;
	}
	public void setNewHardinessE(double newHardinessE){
		hardinessE = newHardinessE;
	}
	public void setNewHardinessP(double newHardinessP){
		hardinessE = newHardinessP;
	}
	public void setDeathCounter(){
		knockoutCounter += 1;
	}
	public int getKnockoutCounter(){
		return knockoutCounter;
	}
}