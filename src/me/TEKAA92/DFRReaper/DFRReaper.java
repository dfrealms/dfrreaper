package me.TEKAA92.DFRReaper;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLevelChangeEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;



public class DFRReaper extends JavaPlugin implements Listener {
	
	static final Logger logger = Logger.getLogger("Minecraft");
	public static DFRReaper plugin; 

	protected DFRReaper log;
	protected HashMap<String, DFRPlayer> register = new HashMap<String, DFRPlayer>();
	public static DFRReaper instance;
	private String[] racesAvailable = { "elf", "human", "orc", "dwarf" };
	DecimalFormat df = new DecimalFormat("#.##");
	private int TotalKnockouts;
	private String knockoutInfo;

	
	
	public static DFRReaper getInstance() {
		return instance;
	}
		

	@Override
	public void onEnable() {
		
		PluginDescriptionFile pdfFile = this.getDescription();
		DFRReaper.logger.info(pdfFile.getName() + " Version " + pdfFile.getVersion() + " Has Been Enabled!");
		getServer().getPluginManager().registerEvents(this, this);
		DFRReaperInitialization();
		getConfig().options().copyDefaults(true);
		saveConfig();
		
		File playersDir = new File(getDataFolder() + File.separator + "players");
		if(!playersDir.exists()){
		    playersDir.mkdir();
		}		
		File infoDir = new File(getDataFolder() + File.separator + "info");
		if(!infoDir.exists()){
		    infoDir.mkdir();
		    File infoFile = new File(this.getDataFolder() + File.separator + "players" + File.separator + knockoutInfo + ".yml");
		    FileConfiguration fc = YamlConfiguration.loadConfiguration(infoFile);
		   
		    try {
				infoFile.createNewFile();
			     fc.set("TotalKnockouts", 0);
			     fc.save(infoFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
		    TotalKnockouts = fc.getInt("TotalKnockouts");
		    
		} else {
		    File infoFile = new File(this.getDataFolder() + File.separator + "players" + File.separator + knockoutInfo + ".yml");
		    FileConfiguration fc = YamlConfiguration.loadConfiguration(infoFile);
			TotalKnockouts = fc.getInt("TotalKnockouts");
		}	
	}
	 
	
	@Override
	public void onDisable() {
		PluginDescriptionFile pdfFile = this.getDescription();
		DFRReaper.logger.info(pdfFile.getName() + " Has Been Disabled!");
		try {
		    File infoFile = new File(this.getDataFolder() + File.separator + "players" + File.separator + knockoutInfo + ".yml");
		    FileConfiguration fc = YamlConfiguration.loadConfiguration(infoFile);
		    fc.set("TotalKnockouts", TotalKnockouts);
		    fc.save(infoFile);
		} catch (Exception e){
		}
	}
	
	public HashMap<String, DFRPlayer> getValues() {
		return this.register;	
		}
	
	
	public void DFRReaperInitialization() {
		
		String dfrReaper = this.getDataFolder().getAbsolutePath();	
		(new File(dfrReaper)).mkdirs(); 
			

	}
				//check OP commands
	
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
		Player player = (Player) sender;
		
		if (sender instanceof Player) {			
			String playerName = player.getName();
			
			if(commandLabel.equalsIgnoreCase("hardiness")) {		
				if (register.containsKey(playerName)) {
					
				DFRPlayer getPlayer = (DFRPlayer)register.get(playerName);
				player.sendMessage(ChatColor.AQUA + "Hardiness Enviroment: " + (df.format(getPlayer.getHardinessE())) + " Hardiness PVP: " + (df.format(getPlayer.getHardinessP())));	

				
				} else {
					player.sendMessage(ChatColor.RED + "Something went wrong" );
					return true;
				} 
			}
			
			else if (commandLabel.equalsIgnoreCase("DFR_set_hardinessE")) {
				if(player.isOp()) {
					if (args.length == 1) { //DFR_set_hardinessE 23
						if (register.containsKey(playerName)) {
							DFRPlayer getPlayer = (DFRPlayer)register.get(playerName);
							double value = Double.parseDouble(args[0]);
						
							getPlayer.setNewHardinessE(value);
							player.sendMessage(ChatColor.AQUA + "Your Hardiness Enviroment have been set to " + value);
					
						}
					
					} else if(args.length == 2) {	
						Player targetPlayer = player.getServer().getPlayer(args[1]);
						if (targetPlayer != null) {

							if (register.containsKey(targetPlayer.getName())) {
								DFRPlayer getTargetPlayer = (DFRPlayer)register.get(targetPlayer);
								double value = Double.parseDouble(args[1]);
						
								getTargetPlayer.setNewHardinessE(value);
								player.sendMessage(ChatColor.AQUA + targetPlayer.getName() + "s Hardiness Enviroment has been set to " + value);
								targetPlayer.sendMessage(ChatColor.AQUA + "Your Hardiness Enviroment have been set to " + value);
							}
						} else  player.sendMessage(ChatColor.RED + "Player do not exist");
					}
				}
			} 
			
			else if (commandLabel.equalsIgnoreCase("DFR_set_hardinessP")) {

				if (args.length == 1) { //DFR_set_hardiness 23
						if (register.containsKey(playerName)) {
							DFRPlayer getPlayer = (DFRPlayer)register.get(playerName); //gets the player from the HashMap
							double value = Double.parseDouble(args[0]); //converts the argument to a double
						
							getPlayer.setNewHardinessP(value);
							player.sendMessage(ChatColor.AQUA + "Your Hardiness PVP have been set to " + value);
						}
					
					} else if(args.length == 2) {	
						Player targetPlayer = player.getServer().getPlayer(args[1]);
						if (targetPlayer != null) {
						String test = player.getServer().getPlayer(args[0]).getName();
					
							if (register.containsKey(targetPlayer.getName())) {
								DFRPlayer getTargetPlayer = (DFRPlayer)register.get(targetPlayer); //gets the targeted player from the HashMap
								double value = Double.parseDouble(args[1]); //converts the argument to a double
						
								getTargetPlayer.setNewHardinessP(value);
								player.sendMessage(ChatColor.AQUA + targetPlayer.getName() + "s Hardiness PVP has been set to " + value);
								targetPlayer.sendMessage(ChatColor.AQUA + "Your Hardiness PVP have been set to " + value);
							}
						} else  player.sendMessage(ChatColor.RED + "Player do not exist");
					}
				} 
			
			else if (commandLabel.equalsIgnoreCase("DFR_set_race")) {
					if (args.length == 1) { //DFR_set_race elf
						if (register.containsKey(playerName)) {
							for (String p : racesAvailable) { //goes through the races
								if (args[0].equalsIgnoreCase(p)) { //checks if the argument the player typed match a race
									DFRPlayer getPlayer = (DFRPlayer)register.get(playerName);
									String value = args[0];
						
									getPlayer.setRace(value);
									player.sendMessage(ChatColor.AQUA + "Your Race have now been set to " + args[0]);
									
								}
							}
						}
					
					} else if(args.length == 2) {	 //DFR_set_race TEKAA92 elf
						Player targetPlayer = player.getServer().getPlayer(args[0]);
						if (targetPlayer != null) {
						String test = targetPlayer.getName();
							if (register.containsKey(test)) {
								for (String p : racesAvailable) { //goes through the races 
									if (args[1].equalsIgnoreCase(p)) { //checks if the argument the player typed match a race
										DFRPlayer getTargetPlayer = (DFRPlayer)register.get(targetPlayer);
										String value = args[1];
						
										getTargetPlayer.setRace(value);
										player.sendMessage(ChatColor.AQUA + targetPlayer.getName() + "s Race has now been set to " + args[1]);
										targetPlayer.sendMessage(ChatColor.AQUA + "Your Race have now been set to " + args[0]);
									}
								}
							}
						} else  player.sendMessage(ChatColor.RED + "Player do not exist");
					}
				} 
			else if (commandLabel.equalsIgnoreCase("TotalDeath")){
				player.sendMessage(ChatColor.GREEN + "Total knockout count: " + TotalKnockouts);
			}
		}
		return false;
	}
	

	@EventHandler 
	public void onPlayerJoin(PlayerJoinEvent event) throws Exception {
		
	Player player = event.getPlayer();
	
		if (player != null) {

		onPlayerLogin(player);
		}
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e){
		
	    if (e.getEntity() instanceof Player){
	        Player player = (Player)e.getEntity(); //This getEntity was PlayerEntity when i tested it, sorry!
	        String test = player.getName();
	        
	        if (register.containsKey(test)) {
	        	if(e.getEntity().getKiller() instanceof Player) {
	        		String killerName = e.getEntity().getKiller().getName();
	            
	            	if (register.containsKey(killerName)) {
	            		player.sendMessage(ChatColor.RED+"You died by player!");
	            		playerDeathPlayer(player);
	            	} 
	            } else {
	        		player.sendMessage(ChatColor.RED+"You died by the enviroment/monster!");
	        		playerDeathEntity(player);
	            } 
	        }
	    }
	}
	
	
	
	FileConfiguration userconfig = null;
	private final int he = getConfig().getInt("Hardiness_Enviroment_First_Login"); //Enviromental Hardiness
	private final int hp = getConfig().getInt("Hardiness_PVP_First_Login"); //PVP Hardiness
	private final String defaultRaceOnJoin = "Human";
	
	public void onPlayerLogin(Player player) throws IOException {
		String playerName = player.getName();
		File playerFile = new File(this.getDataFolder() + File.separator + "players" + File.separator + playerName + ".yml");
		FileConfiguration fc = YamlConfiguration.loadConfiguration(playerFile);		
		
			
			if(!playerFile.exists()){
				int lvlPlayer = player.getLevel();
				
			     playerFile.createNewFile(); // needs try catch block
			     fc.set("Name", playerName);
			     fc.set("HardinessEnviroment", he);
			     fc.set("HardinessPvP", hp);
			     fc.set("CurrentLevel", lvlPlayer);
			     fc.set("Race", defaultRaceOnJoin);
			     fc.set("Knockouts", 0);
			     fc.save(playerFile);
			     
				register.put(playerName, new DFRPlayer(playerName, he, hp, lvlPlayer, defaultRaceOnJoin, 0));	
				DFRPlayer getPlayer = (DFRPlayer)register.get(playerName);
				setHardiness(getPlayer, player);
			
				player.sendMessage(ChatColor.AQUA + "You have: " + (df.format(getPlayer.getHardinessE())) + " Hardiness Enviroment and: " + (df.format(getPlayer.getHardinessP())) + " Hardiness PvP " + playerName);	
			} else {
							
				if (register.containsKey(playerName)) {	
					
					DFRPlayer getPlayer = (DFRPlayer)register.get(playerName);
					
					player.sendMessage(ChatColor.AQUA + "You have: " +  (df.format(getPlayer.getHardinessE())) + " Hardiness Enviroment and: " + (df.format(getPlayer.getHardinessP())) + " Hardiness PvP");
						
				} else {
					String configName = fc.getString("Name");
					double configHE = fc.getDouble("HardinessEnviroment");
					double configHP = fc.getDouble("HardinessPvP");
					int configLvlPlayer = fc.getInt("CurrentLevel");
					String configRace = fc.getString("Race");
					int configKnockout = fc.getInt("Knockouts");
					register.put(playerName, new DFRPlayer(configName, configHE, configHP, configLvlPlayer, configRace, configKnockout));
					DFRPlayer getPlayer = (DFRPlayer)register.get(playerName);
					DFRReaper.logger.info("[DFRReaper] " + playerName + " loaded");
					player.sendMessage(ChatColor.AQUA + "You have: " +  (df.format(getPlayer.getHardinessE())) + " Hardiness Enviroment and: " + (df.format(getPlayer.getHardinessP())) + " Hardiness PvP");
				}
			}
			       
		} //Test end!
	
	
	
	@EventHandler
	public void onPlayerLvlUp(PlayerLevelChangeEvent p) {
		int lvlPlayer = p.getPlayer().getLevel();
		Player player = p.getPlayer();
		
		if(player != null) {
			String playerName = player.getName();	
			DFRPlayer getPlayer = (DFRPlayer)register.get(playerName);
			
			if(lvlPlayer > getPlayer.getlvlPlayer()) {
				if (getPlayer.getDFRRace().equalsIgnoreCase("Human")) {
					double lvlE = getConfig().getDouble("Human_lvlE");
					double lvlP = getConfig().getDouble("Human_lvlP");
					getPlayer.lvlPlayerUp(lvlPlayer);
					getPlayer.hardinessElvl(lvlE);
					getPlayer.hardinessPlvl(lvlP);
				}
				else if (getPlayer.getDFRRace().equalsIgnoreCase("Elf")) {
					double lvlE = getConfig().getDouble("Elf_lvlE");
					double lvlP = getConfig().getDouble("Elf_lvlP");
					getPlayer.lvlPlayerUp(lvlPlayer);
					getPlayer.hardinessElvl(lvlE);
					getPlayer.hardinessPlvl(lvlP);
				}
				else if (getPlayer.getDFRRace().equalsIgnoreCase("Orc")) {
					double lvlE = getConfig().getDouble("Orc_lvlE");
					double lvlP = getConfig().getDouble("Orc_lvlP");
					getPlayer.lvlPlayerUp(lvlPlayer);
					getPlayer.hardinessElvl(lvlE);
					getPlayer.hardinessPlvl(lvlP);
				}
				else if (getPlayer.getDFRRace().equalsIgnoreCase("Dwarf")) {
					double lvlE = getConfig().getDouble("Dwarf_lvlE");
					double lvlP = getConfig().getDouble("Dwarf_lvlP");
					getPlayer.lvlPlayerUp(lvlPlayer);
					getPlayer.hardinessElvl(lvlE);
					getPlayer.hardinessPlvl(lvlP);
				}
			}	
		}
	}
	

	public void playerDeathPlayer(final Player player) {
		String playerName = player.getName();
		double death = getConfig().getDouble("Death");
		double lvlDeath = getConfig().getDouble("PercentOfLevelLoss");
		
		DFRPlayer getPlayer = (DFRPlayer)register.get(playerName);
		getPlayer.lvlPlayerDeath(lvlDeath);
		
		if (getPlayer.getHardinessP() <= 0) {
			
			setHardiness(getPlayer, player);
			player.sendMessage(ChatColor.RED + "You have now reached 0 H. New life started:" + (df.format(getPlayer.getHardinessP())) + " Hardiness PvP left");	
			
		} else {
			getPlayer.setHardinessP(death);
			player.sendMessage(ChatColor.RED + "You have: " + (df.format(getPlayer.getHardinessP())) + " Hardiness PvP left");
		}
	}
		 
	
	public void playerDeathEntity(final Player player) {
		String playerName = player.getName();
		double death = getConfig().getDouble("Death");
		double lvlDeath = getConfig().getDouble("PercentOfLevelLoss");
		TotalKnockouts += 1;
		
		DFRPlayer getPlayer = (DFRPlayer)register.get(playerName);
		
		if (getPlayer.getHardinessE() <= 0) {	
			getPlayer.lvlPlayerDeath(lvlDeath);
			getPlayer.setDeathCounter();
			setHardiness(getPlayer, player);
			player.sendMessage(ChatColor.RED + "You have now reached 0 H. New life started: " + (df.format(getPlayer.getHardinessE())) + " Hardiness Enviroment left");	
		
		} else {		
			getPlayer.setHardinessE(death);
			getPlayer.setDeathCounter();
			player.sendMessage(ChatColor.RED + "You have: " + (df.format(getPlayer.getHardinessE())) + " Hardiness Enviroment left");
		}
	}
	
	public void setHardiness(DFRPlayer getPlayer, Player player) { //Will add new hardiness depending on level
		
		int level = getPlayer.getlvlPlayer();
		//float removeExperience = (player.getTotalExperience() / 4);
		//player.setExp(player.getTotalExperience() - removeExperience);
	
		if (getPlayer.getDFRRace().equalsIgnoreCase("Human")) {
			double lvlE = getConfig().getDouble("Human_lvlE");
			double lvlP = getConfig().getDouble("Human_lvlP");
			double newLevelE = lvlE * level; //Setting the new levelE
			double newLevelP = lvlP * level; //Setting the new levelP
			getPlayer.hardinessElvl(newLevelE);
			getPlayer.hardinessPlvl(newLevelP);
		} 
		else if (getPlayer.getDFRRace().equalsIgnoreCase("Elf")) {
			double lvlE = getConfig().getDouble("Elf_lvlE");
			double lvlP = getConfig().getDouble("Elf_lvlP");
			double newLevelE = lvlE * level; //Setting the new level
			double newLevelP = lvlP * level; //Setting the new levelP
			getPlayer.hardinessElvl(newLevelE);
			getPlayer.hardinessPlvl(newLevelP);
		}
		else if (getPlayer.getDFRRace().equalsIgnoreCase("Orc")) {
			double lvlE = getConfig().getDouble("Orc_lvlE");
			double lvlP = getConfig().getDouble("Orc_lvlP");
			double newLevelE = lvlE * level; //Setting the new level
			double newLevelP = lvlP * level; //Setting the new levelP
			getPlayer.hardinessElvl(newLevelE);
			getPlayer.hardinessPlvl(newLevelP);
		}
		else if (getPlayer.getDFRRace().equalsIgnoreCase("Dwarf")) {
			double lvlE = getConfig().getDouble("Dwarf_lvlE");
			double lvlP = getConfig().getDouble("Dwarf_lvlP");
			double newLevelE = lvlE * level; //Setting the new level
			double newLevelP = lvlP * level; //Setting the new levelP
			getPlayer.hardinessElvl(newLevelE);
			getPlayer.hardinessPlvl(newLevelP);
		}		
	}
	
	@EventHandler
	public void onPlayerLogout(PlayerQuitEvent event) throws Exception {
        
		Player player = event.getPlayer();
        if (player != null) {
        	saveConfigurationDFR(player);
        }
	}
	
	public void saveConfigurationDFR(Player player) throws Exception {
	
        	String name = player.getName();
        	DFRPlayer getPlayer = (DFRPlayer)register.get(name);
    		File playerFile = new File(this.getDataFolder() + File.separator + "players" + File.separator + name + ".yml");
    		FileConfiguration fc = YamlConfiguration.loadConfiguration(playerFile);	
    		
		     fc.set("Name", getPlayer.getDFRPlayerName());
		     fc.set("HardinessEnviroment", getPlayer.getHardinessE());
		     fc.set("HardinessPvP", getPlayer.getHardinessP());
		     fc.set("CurrentLevel", getPlayer.getlvlPlayer());
		     fc.set("Race", getPlayer.getDFRRace());
		     fc.set("Knockouts", getPlayer.getKnockoutCounter());
		     fc.save(playerFile);
		     DFRReaper.logger.info("[DFRReaper] " + name + " saved");
        
	 }
}
